public class Multiplikation {
	public static void main(String[] args) {
		double a = 2.0;
		System.out.println(multi(a, 3));
		System.out.println (volumenWuerfel(2));
		System.out.println (volumenQuader(1,4,6));
		System.out.println (volumenPyramide (2,4));
		System.out.println (volumenKugel(4));
	}
	
	public static double multi(double zahl1, double zahl2) {
		double produkt = zahl1 * zahl2;
		return produkt;
	}
	
	public static double volumenWuerfel(double seite) {
		double volumen = seite * seite * seite;   
		return volumen; 
	}
	
	public static double volumenQuader(double zahlA, double zahlB, double zahlC) {
		double volumen = zahlA * zahlB * zahlC; 
		return volumen;
	}
	
	public static double volumenPyramide(double zahlA, double zahlH) {
		double volumen = zahlA * zahlA * zahlH / 3;
		return volumen;
	}
	public static double volumenKugel (double radius) {
		double volumen = 4/3 * (radius * radius * radius) * Math.PI;
		return volumen;
	}
}