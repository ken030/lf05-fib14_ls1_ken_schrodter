class Konsolenausgabe1 {

	public static void main(String[] args) {

		System.out.println("Hallo ich bin in der Schule " + " \n \"Ich programmiere hier\"");
		//println macht einen Zeilenumgbruch, print macht keinen Zeilenumbruch
		
		System.out.println("        ");
		
		System.out.println("        *");
		System.out.println("       ***");
		System.out.println("      *****");
		System.out.println("     *******");
		System.out.println("    *********");
		System.out.println("   ***********");
		System.out.println("       ***");
		System.out.println("       ***");
		
		System.out.println("        ");
		
		System.out.println("        ");
		
		double d =224234234;
		double e =111.2222; 
		double f =4.0;
		double g=1000000.551;	
		double h=97.34;

		System.out.printf( "%.2f\n" ,d);
		System.out.printf( "%.2f\n" ,e);
		System.out.printf( "%.2f\n" ,f);
		System.out.printf( "%.2f\n" ,g);
		System.out.printf( "%.2f\n" ,h);
		System.out.println("        ");
		
		System.out.println("  **");
		System.out.println("*"+"    "+"*");
		System.out.println("*"+"    "+"*");
		System.out.println("  **");
		
		System.out.println("        ");
		
		
	}
}
